'use strict';

const auth = require('feathers-authentication');
const SteamStrategy = require('passport-steam');
const util = require('util');

module.exports = function() {
  var app = this;
  console.log(util.inspect(app, {depth: null}));
  // Add a logger to our app object for convenience
  let config = app.get('steam');
  /*
  app.passport.use(new SteamStrategy({
      returnURL: config.returnURL,
      realm: config.realm,
      apiKey: config.apiKey
    },
    function(identifier, profile, done) {
      const query = { openId: identifier };
      app.service.find({ query }).then(response => {
        const user = response[0];
        return done(null, user);
      }).catch(done);
    }
  ));

  app.get('/auth/steam', auth.express.authenticate('steam'));

  app.get('/auth/steam/return',
    auth.express.authenticate('steam', { failureRedirect: '/login' }),
    function(req, res) {
      // Successful authentication, redirect home.
      res.redirect('/');
    });
    */
};
