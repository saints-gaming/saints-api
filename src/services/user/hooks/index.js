'use strict';


const gravatar = require('./gravatar');
const local = require('feathers-authentication-local').hooks;
const globalHooks = require('../../../hooks');
const hooks = require('feathers-hooks');
const auth = require('feathers-authentication').hooks;

exports.before = {
  all: [],
  find: [
    auth.authenticate('jwt')
    //auth.populateUser(),
    //auth.restrictToAuthenticated(),
  ],
  get: [
    auth.authenticate('jwt'),
    //auth.populateUser(),
    //auth.restrictToAuthenticated(),
    //auth.restrictToOwner({ ownerField: '_id' })
  ],
  create: [
    local.hashPassword({ passwordField: 'password' }),
    gravatar()
  ],
  update: [
    auth.authenticate('jwt'),
    //auth.populateUser(),
    //auth.restrictToAuthenticated(),
    //auth.restrictToOwner({ ownerField: '_id' }),
    gravatar()
  ],
  patch: [
    auth.authenticate('jwt'),
    //auth.populateUser(),
    //auth.restrictToAuthenticated(),
    //auth.restrictToOwner({ ownerField: '_id' }),
    gravatar()
  ],
  remove: [
    auth.authenticate('jwt'),
    //auth.populateUser(),
    //auth.restrictToAuthenticated(),
    //auth.restrictToOwner({ ownerField: '_id' })
  ]
};

exports.after = {
  all: [hooks.remove('password')],
  find: [],
  get: [],
  create: [],
  update: [],
  patch: [],
  remove: []
};
