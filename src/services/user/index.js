'use strict';

const service = require('feathers-mongoose');
const user = require('./user-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: user,
    lean: true,
    paginate: {
      default: 5,
      max: 25
    }
  };

  const Service = service(options);

  Service.docs = {
    description: 'Handles user functions',
    definitions: {
      users: {
        "type": "object",
        "required": [
          "email",
          "password"
        ],
        "unique": [
          "email"
        ],
        "properties": {
          "facebookId": {
            "type": "string",
            "description": "Facebook ID"
          },
          "facebook": {
            "type": "string",
            "description": "Facebook private key"
          },
          "googleId": {
            "type": "string",
            "description": "Google ID"
          },
          "google": {
            "type": "string",
            "description": "Google private key"
          },
          "instagramId": {
            "type": "string",
            "description": "instagram ID"
          },
          "instagram": {
            "type": "string",
            "description": "instagram private key"
          },
          "paypalId": {
            "type": "string",
            "description": "paypal ID"
          },
          "paypal": {
            "type": "string",
            "description": "paypal private key"
          },
          "email": {
            "type": "string",
            "description": "User email"
          },
          "password": {
            "type": "string",
            "description": "User Password"
          }
        }
      }
    }
  };

  // Initialize our service with any options it requires
  app.use('/users', Service);

  // Get our initialize service to that we can bind hooks
  const userService = app.service('/users');

  // Set up our before hooks
  userService.before(hooks.before);

  // Set up our after hooks
  userService.after(hooks.after);
};
