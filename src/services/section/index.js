'use strict';

const service = require('feathers-mongoose');
const section = require('./section-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: section,
    lean: true,
    paginate: {
      default: 5,
      max: 25
    }
  };

  const Service = service(options);

  Service.docs = {
    description: 'Major sections of the forum',
    definitions: {
      sections: {
        "type": "object",
        "required": [
          "name",
          "key"
        ],
        "unique": [
          "key"
        ],
        "properties": {
          "name": {
            "type": "string",
            "description": "The sections name/title"
          },
          "key": {
            "type": "string",
            "description": "The sections key"
          }
        }
      }
    }
  };

  // Initialize our service with any options it requires
  app.use('/sections', Service);

  // Get our initialize service to that we can bind hooks
  const sectionService = app.service('/sections');

  // Set up our before hooks
  sectionService.before(hooks.before);

  // Set up our after hooks
  sectionService.after(hooks.after);
};
