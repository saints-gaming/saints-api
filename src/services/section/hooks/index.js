'use strict';

const globalHooks = require('../../../hooks');
const hooks = require('feathers-hooks');
const auth = require('feathers-authentication').hooks;

exports.before = {
  all: [],
  find: [],
  get: [],
  create: [
    auth.authenticate('jwt'),
    //auth.populateUser(),
    //auth.restrictToAuthenticated(),
    //auth.restrictToRoles({
    //  roles: ['admin', 'super-admin']
    //})
  ],
  update: [
    auth.authenticate('jwt'),
    //auth.populateUser(),
    //auth.restrictToAuthenticated(),
    //auth.restrictToRoles({
    //  roles: ['admin', 'super-admin']
    //})
  ],
  patch: [
    auth.authenticate('jwt'),
    //auth.populateUser(),
    //auth.restrictToAuthenticated(),
    //auth.restrictToRoles({
    //  roles: ['admin', 'super-admin']
    //})
  ],
  remove: [
    auth.authenticate('jwt'),
    //auth.populateUser(),
    //auth.restrictToAuthenticated(),
    //auth.restrictToRoles({
    //  roles: ['admin', 'super-admin']
    //})
  ]
};

exports.after = {
  all: [],
  find: [],
  get: [],
  create: [],
  update: [],
  patch: [],
  remove: []
};
