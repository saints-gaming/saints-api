'use strict';

const service = require('feathers-mongoose');
const thread = require('./thread-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: thread,
    paginate: {
      default: 100,
      max: 500
    }
  };

  // Initialize our service with any options it requires
  app.use('/threads', service(options));

  // Get our initialize service to that we can bind hooks
  const threadService = app.service('/threads');

  // Set up our before hooks
  threadService.before(hooks.before);

  // Set up our after hooks
  threadService.after(hooks.after);
};
