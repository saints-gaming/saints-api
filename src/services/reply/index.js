'use strict';

const service = require('feathers-mongoose');
const reply = require('./reply-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: reply,
    paginate: {
      default: 5,
      max: 25
    }
  };

  // Initialize our service with any options it requires
  app.use('/replies', service(options));

  // Get our initialize service to that we can bind hooks
  const replyService = app.service('/replies');

  // Set up our before hooks
  replyService.before(hooks.before);

  // Set up our after hooks
  replyService.after(hooks.after);
};
