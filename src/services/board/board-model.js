'use strict';

// board-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const boardSchema = new Schema({
  name: { type: String, required: true },
  key: { type: String, required: true },
  description: {type: String},
  section: {type: Schema.Types.ObjectId, ref: 'sections'},
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

boardSchema.index({key: 1, section:1}, {unique: true});

const boardModel = mongoose.model('board', boardSchema);

module.exports = boardModel;
