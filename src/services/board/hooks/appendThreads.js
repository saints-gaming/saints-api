'use strict';

// src/services/board/hooks/appendThreads.js
//
// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/hooks/readme.html
const inspect = require('util').inspect;
const defaults = {};

module.exports = function(options) {
  options = Object.assign({}, defaults, options);

  return function(hook) {
    const threadService = hook.app.service('/threads');
    let promises = [];
    hook.result.data.forEach(board => {
      promises.push(new Promise((resolve, reject) => {
        threadService
          .find({query: {board: board._id}})
          .then(result => {
            board.threads = result;
            resolve(board)
          }, error => reject(error))
      }))
    });

    return Promise
      .all(promises)
      .then(result => {
        hook.result.data = result;
        return hook;
      });
  };
};
