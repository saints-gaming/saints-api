'use strict';
const util = require('util');
const mongoose = require('mongoose');
const defaults = {};

module.exports = function(options) {
  options = Object.assign({}, defaults, options);

  return function(hook) {
    const query = hook.params.query ? hook.params.query : {};
    const ObjectId= mongoose.Types.ObjectId;
    if(query.section && (ObjectId.isValid(query.section) ? (new Date(ObjectId(query.section).getTimestamp()).toString()) == "Invalid Date" : true)) {
      return hook.app.service('/sections')
        .find({query: {key: query.section}})
        .then(result => {
          const section = result.data[0];
          hook.params.query = Object.assign({}, query, {section: section._id});
          return hook;
        })
    }
  };
};
