'use strict';

const appendThreads = require('./appendThreads');

const resolveSection = require('./resolveSection');


const globalHooks = require('../../../hooks');
const hooks = require('feathers-hooks-common');
const auth = require('feathers-authentication').hooks;
const permissions = require('feathers-permissions').hooks;

const afterSchema = {
  service: '/boards',
  include: [
    {
      service: '/sections',
      nameAs: 'section',
      parentField: 'section',
      query: {
        $select: ['name', 'key', '_id']
      },
      childField: '_id',
    },
  ],
};
const onlyAdmin = {
  permissions: ['admin', 'super-admin'],
  on: 'user',
  field: 'role'
};
exports.before = {
  all: [],
  find: [resolveSection()],
  get: [resolveSection()],
  create: [
    auth.authenticate('jwt'),
    permissions.checkPermissions(onlyAdmin),
    permissions.isPermitted()
    //auth.populateUser(),
    //auth.restrictToAuthenticated(),
    //auth.restrictToRoles({
    //  roles: ['admin', 'super-admin']
    //})
  ],
  update: [
    auth.authenticate('jwt'),
    //auth.populateUser(),
    //auth.restrictToAuthenticated(),
    //auth.restrictToRoles({
    //  roles: ['admin', 'super-admin']
    //})
  ],
  patch: [
    auth.authenticate('jwt'),
    //auth.populateUser(),
    //auth.restrictToAuthenticated(),
    //auth.restrictToRoles({
    //  roles: ['admin', 'super-admin']
    //})
  ],
  remove: [
    auth.authenticate('jwt'),
    //auth.populateUser(),
    //auth.restrictToAuthenticated(),
    //auth.restrictToRoles({
    //  roles: ['admin', 'super-admin']
    //})
  ]
};

exports.after = {
  all: [hooks.populate({ schema: afterSchema, profile: false })],
  find: [appendThreads()],
  get: [appendThreads()],
  create: [],
  update: [],
  patch: [],
  remove: []
};
