'use strict';

const service = require('feathers-mongoose');
const board = require('./board-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: board,
    lean:true,
    paginate: {
      default: 100,
      max: 250
    }
  };

  const Service = service(options);

  Service.docs = {
    description: 'Forum boards/categories',
    definitions: {
      boards: {
        "type": "object",
        "required": [
          "name",
          "key"
        ],
        "unique": [
        ],
        "properties": {
          "name": {
            "type": "string",
            "description": "Boards name"
          },
          "key": {
            "type": "string",
            "description": "Boards URL key"
          },
          "description": {
            "type": "string",
            "description": "Boards description"
          },
          "section": {
            "type": "string",
            "description": "ObjectId of related section"
          }
        }
      }
    }
  };

  // Initialize our service with any options it requires
  app.use('/boards', Service);

  // Get our initialize service to that we can bind hooks
  const boardService = app.service('/boards');

  // Set up our before hooks
  boardService.before(hooks.before);

  // Set up our after hooks
  boardService.after(hooks.after);
};
