'use strict';

const auth = require('feathers-authentication');

const local = require('feathers-authentication-local');
const jwt = require('feathers-authentication-jwt');
//const oauth1 = require('feathers-authentication-oauth1');
//const SteamStrategy = require('passport-steam').Strategy;

module.exports = function() {
  const app = this;

  let config = app.get('auth');

  app.set('auth', config);
  app
    .configure(auth(config))
    .configure(jwt())
    .configure(local())
    //.configure(oauth1())

    .service('authentication').hooks({
      before: {
        create: [
          auth.hooks.authenticate(['jwt', 'local'])
        ]
      }
    })
};
