'use strict';
const reply = require('./reply');
const thread = require('./thread');
const board = require('./board');
const section = require('./section');
const authentication = require('./authentication');
const user = require('./user');
//const steam = require('./steam');
const mongoose = require('mongoose');
module.exports = function() {
  const app = this;

  mongoose.connect(app.get('mongodb'));
  mongoose.Promise = global.Promise;

  app.configure(authentication);
  //app.configure(steam);
  app.configure(user);
  app.configure(section);
  app.configure(board);
  app.configure(thread);
  app.configure(reply);
};
