'use strict';

const assert = require('assert');
const appendThreads = require('../../../../src/services/board/hooks/appendThreads.js');

describe('board appendThreads hook', function() {
  it('hook can be used', function() {
    const mockHook = {
      type: 'after',
      app: {},
      params: {},
      result: {},
      data: {}
    };

    appendThreads()(mockHook);

    assert.ok(mockHook.appendThreads);
  });
});
