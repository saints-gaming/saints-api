'use strict';

const assert = require('assert');
const resolveSection = require('../../../../src/services/board/hooks/resolveSection.js');

describe('board resolveSection hook', function() {
  it('hook can be used', function() {
    const mockHook = {
      type: 'before',
      app: {},
      params: {},
      result: {},
      data: {}
    };

    resolveSection()(mockHook);

    assert.ok(mockHook.resolveSection);
  });
});
