'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('reply service', function() {
  it('registered the replies service', () => {
    assert.ok(app.service('replies'));
  });
});
