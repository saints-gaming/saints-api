'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('thread service', function() {
  it('registered the threads service', () => {
    assert.ok(app.service('threads'));
  });
});
